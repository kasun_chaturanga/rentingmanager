﻿using RentingManager.Client.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentingManager.Client.Models.Item
{
    public class ItemTypePagedListModel
    {
        public ItemTypePagedListModel()
        {
            List = new List<ItemTypeModel>();
        }

        public IList<ItemTypeModel> List { get; set; }

        public PagerModel PagerModel { get; set; }
    }
}
