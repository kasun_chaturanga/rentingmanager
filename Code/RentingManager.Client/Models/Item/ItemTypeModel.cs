﻿namespace RentingManager.Client.Models.Item
{
    public class ItemTypeModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
