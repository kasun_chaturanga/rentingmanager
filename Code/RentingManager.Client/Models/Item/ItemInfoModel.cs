﻿using RentingManager.Server.Core.Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RentingManager.Client.Models.Item
{
    public class ItemInfoModel
    {
        public ItemInfoModel()
        {
            Rates = new List<RentalRateModel>();          
        }

        public int Id { get; set; }

        public int SelectedItemTypeId { get; set; }

        [Display(Name = "Item Type")]
        public IList<ItemType> AvailableItemTypes { get; set; }

        public string Name { get; set; }

        [Display(Name = "Idetification Number")]
        public string IdetificationNumber { get; set; }

        [Display(Name = "Model Number")]
        public string ModelNumber { get; set; }

        public string Description { get; set; }

        public IList<RentalRateModel> Rates { get; set; }
    }
}
