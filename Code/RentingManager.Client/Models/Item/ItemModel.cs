﻿namespace RentingManager.Client.Models.Item
{
    public class ItemModel
    {
        public int Id { get; set; }

        public string ItemType { get; set; }

        public string Name { get; set; }

        public string IdetificationNumber { get; set; }

        public string ModelNumber { get; set; }

        public string Description { get; set; }
    }
}
