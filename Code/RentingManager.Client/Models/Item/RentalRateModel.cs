﻿namespace RentingManager.Client.Models.Item
{
    public class RentalRateModel
    {
        public int Id { get; set; }

        public string RentalRateType { get; set; }

        public decimal Rate { get; set; }

        public string DisplayLabel { get; set; }
    }
}
