﻿using RentingManager.Client.Models.Common;
using RentingManager.Server.Core.Domain;
using System.Collections.Generic;

namespace RentingManager.Client.Models.Item
{
    public class ItemPagedListModel
    {
        public ItemPagedListModel()
        {
            List = new List<ItemModel>();
        }

        public IList<ItemModel> List { get; set; }

        public PagerModel PagerModel { get; set; }

        public IList<ItemType> AvailableItemTypes { get; set; }
    }
}
