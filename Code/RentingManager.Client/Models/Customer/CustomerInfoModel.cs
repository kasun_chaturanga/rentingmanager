﻿namespace RentingManager.Client.Models.Customer
{
    /// <summary>
    /// Represent customer information.
    /// </summary>
    public class CustomerInfoModel
    {
        public int Id { get; set; }
    
        public string Name { get; set; }

        public string Nic { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }
    }
}
