﻿using RentingManager.Client.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentingManager.Client.Models.Customer
{
    public class CustomerInfoPagedListModel
    {
        public CustomerInfoPagedListModel()
        {
            List = new List<CustomerInfoModel>();
        }

        public IList<CustomerInfoModel> List { get; set; }

        public PagerModel PagerModel { get; set; }

        public string SearchByNicString { get; set; }

        public string SearchByNameString { get; set; }
    }
}
