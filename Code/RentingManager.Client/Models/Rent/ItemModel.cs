﻿using RentingManager.Server.Core.Domain;
using System.Collections.Generic;

namespace RentingManager.Client.Models.Rent
{
    public class ItemModel
    {
        public ItemModel()
        {
            Rates = new List<RateModel>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string IdetificationNumber { get; set; }

        public string ModelNumber { get; set; }

        public IList<RateModel> Rates { get; set; }

        public bool Selected { get; set; }
    }
}
