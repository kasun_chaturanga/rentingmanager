﻿using RentingManager.Server.Core.Domain;
using System;

namespace RentingManager.Client.Models.Rent
{
    public class RentItemModel
    {
        public int ItemId { get; set; }

        public string ItemName { get; set; }

        public RentalRateType RateType { get; set; }

        public decimal GivenRate { get; set; }

        public DateTime RentingDate { get; set; }
    }
}
