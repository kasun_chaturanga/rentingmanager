﻿namespace RentingManager.Client.Models.Rent
{
    public class RentStatusModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
