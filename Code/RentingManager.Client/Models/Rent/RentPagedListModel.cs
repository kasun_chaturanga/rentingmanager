﻿using RentingManager.Client.Models.Common;
using System.Collections.Generic;

namespace RentingManager.Client.Models.Rent
{
    public class RentPagedListModel
    {
        public RentPagedListModel()
        {
            List = new List<RentModel>();
            StatusList = new List<RentStatusModel>();
        }

        public IList<RentModel> List { get; set; }

        public IList<RentStatusModel> StatusList { get; set; }

        public PagerModel PagerModel { get; set; }
    }
}
