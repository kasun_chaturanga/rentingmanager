﻿using RentingManager.Server.Core.Domain;
using System.Collections.Generic;

namespace RentingManager.Client.Models.Rent
{
    public class ItemPopupModel
    {
        public ItemPopupModel()
        {
            AvailableItems = new List<ItemModel>();
        }

        public IList<ItemType> AvailableItemTypes { get; set; }

        public IList<ItemModel> AvailableItems { get; set; }
    }
}
