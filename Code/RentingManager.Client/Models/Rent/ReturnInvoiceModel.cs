﻿using RentingManager.Server.Core.Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RentingManager.Client.Models.Rent
{
    public class ReturnInvoiceModel
    {
        [Display(Name = "Reference Number")]
        public string ReferenceNumber { get; set; }

        [Display(Name = "Customer Id")]
        public int CustomerId { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        public IList<InvoiceItem> Items { get; set; }

        [Display(Name = "Total Amount")]
        public decimal TotalAmount { get; set; }

        [Display(Name = "Paid Amount")]
        public decimal PaidAmount { get; set; }

        [Display(Name = "Balance Amount")]
        public decimal BalanceAmount { get; set; }
    }
}
