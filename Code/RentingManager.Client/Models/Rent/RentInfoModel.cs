﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RentingManager.Client.Models.Rent
{
    public class RentInfoModel
    {
        public RentInfoModel()
        {
            ItemPopup = new ItemPopupModel();
            RentItems = new List<RentItemModel>();
        }

        public string ReferenceNumber { get; set; }

        public int CustomerId { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        [Display(Name = "Renting Date")]
        public DateTime RentingDate { get; set; }

        [Display(Name = "Advance Payment")]
        public decimal AdvancePayment { get; set; }

        public string Description { get; set; }

        public IList<RentItemModel> RentItems { get; set; }

        public ItemPopupModel ItemPopup { get; set; }
    }
}
