﻿namespace RentingManager.Client.Models.Rent
{
    public class RateModel
    {
        public string RentalRateType { get; set; }

        public decimal Rate { get; set; }
    }
}
