﻿using RentingManager.Server.Core.Domain;
using System;

namespace RentingManager.Client.Models.Rent
{
    public class ReturnItemModel
    {
        public int Id { get; set; }

        public int ItemId { get; set; }

        public string ItemName { get; set; }

        public RentalRateType RateType { get; set; }

        public decimal GivenRate { get; set; }

        public DateTime RentingDate { get; set; }

        public bool Returned { get; set; }
    }
}
