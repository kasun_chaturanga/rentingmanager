﻿using RentingManager.Server.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RentingManager.Client.Models.Rent
{
    public class ReturnInfoModel
    {
        public ReturnInfoModel()
        {
            ReturnItems = new List<ReturnItemModel>();
            PreviousPayments = new List<Payment>();
        }

        public int RentId { get; set; }

        public string ReferenceNumber { get; set; }

        [Display(Name = "Return Date")]
        public DateTime ReturnDate { get; set; }

        public IList<ReturnItemModel> ReturnItems { get; set; }

        public IList<Payment> PreviousPayments { get; set; }
    }
}
