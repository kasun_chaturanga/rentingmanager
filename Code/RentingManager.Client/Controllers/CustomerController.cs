﻿using Microsoft.AspNetCore.Mvc;
using RentingManager.Client.Models.Common;
using RentingManager.Client.Models.Customer;
using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using RentingManager.Server.Services;
using System.Collections.Generic;

namespace RentingManager.Client.Controllers
{
    public class CustomerController : Controller
    {
        private ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CustomerInfoModel model)
        {
            var customer = new Customer()
            {
                Name = model.Name.Trim(),
                Nic = model.Nic.Trim(),
                AddressLine1 = model.AddressLine1.Trim(),
                AddressLine2 = model.AddressLine2.Trim(),
                AddressLine3 = model.AddressLine3.Trim()
            };

            try
            {
                _customerService.SaveCustomer(customer);
                TempData["SuccessMessage"] = "Customer saved successfully";
                return RedirectToAction("CustomerList");

            }
            catch
            {
                TempData["ErrorMessage"] = "Error occured.";
            }
            
            return View();
        }

        public ActionResult CustomerList(int pageNumber, string searchByNicString, string searchByNameString)
        {
            var pageSize = 2;
            var pageIndex = 0;
            if (pageNumber > 0)
            {
                pageIndex = pageNumber - 1;
            }
            
            CustomerInfoPagedListModel model = new CustomerInfoPagedListModel();
            IPagedList<Customer> customers = _customerService.GetCustomers(searchByNicString?.Trim(), searchByNameString?.Trim(), pageIndex, pageSize);
            foreach (var customer in customers)
            {
                CustomerInfoModel customerInfoModel = new CustomerInfoModel()
                {
                    Id = customer.Id,
                    Name = customer.Name,
                    Nic = customer.Nic,
                    AddressLine1 = customer.AddressLine1,
                    AddressLine2 = customer.AddressLine2,
                    AddressLine3 = customer.AddressLine3
                };

                model.List.Add(customerInfoModel);
            }

            model.PagerModel = new PagerModel
            {
                PageSize = customers.PageSize,
                TotalRecords = customers.TotalCount,
                PageIndex = customers.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "CustomerList",
                UseRouteLinks = false,
                RouteValues = new CustomerListRouteValues { pageNumber = pageNumber
                        , searchByNicString = searchByNicString, searchByNameString = searchByNameString }
            };

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            IList<CustomerInfoModel> model = new List<CustomerInfoModel>();
            Customer customer = _customerService.GetCustomer(id);
            CustomerInfoModel customerInfoModel = new CustomerInfoModel()
            {
                Id = customer.Id,
                Name = customer.Name,
                Nic = customer.Nic,
                AddressLine1 = customer.AddressLine1,
                AddressLine2 = customer.AddressLine2,
                AddressLine3 = customer.AddressLine3
            };

            return View(customerInfoModel);
        }

        [HttpPost]
        public ActionResult Edit(CustomerInfoModel model)
        {
            var customer = new Customer()
            {
                Id = model.Id,
                Name = model.Name.Trim(),
                Nic = model.Nic.Trim(),
                AddressLine1 = model.AddressLine1.Trim(),
                AddressLine2 = model.AddressLine2.Trim(),
                AddressLine3 = model.AddressLine3.Trim()
            };

            try
            {
                _customerService.UpdateCustomer(customer);
                TempData["SuccessMessage"] = "Customer updated successfully";
                return RedirectToAction("CustomerList");

            }
            catch
            {
                TempData["ErrorMessage"] = "Error occured.";
            }
            
            return View();
        }

        public ActionResult Delete(int id)
        {
            try
            {
                _customerService.DeleteCustomer(id);
                TempData["SuccessMessage"] = "Customer deleted successfully";
                return RedirectToAction("CustomerList");

            }
            catch
            {
                TempData["ErrorMessage"] = "Error occured.";
                return View();
            }
        }
    }
}