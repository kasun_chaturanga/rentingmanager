﻿using Microsoft.AspNetCore.Mvc;
using RentingManager.Client.Models.Common;
using RentingManager.Client.Models.Rent;
using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using RentingManager.Server.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RentingManager.Client.Controllers
{
    public class RentController : Controller
    {
        private ICustomerService _customerService;

        private IItemService _itemService;

        private IRentService _rentService;

        public RentController(ICustomerService customerService, IItemService itemService, IRentService rentService)
        {
            _customerService = customerService;
            _itemService = itemService;
            _rentService = rentService;
        }

        public IActionResult Create(int customerId, int searchTypeId)
        {
            var model = new RentInfoModel();
            model.CustomerId = customerId;
            var customer = _customerService.GetCustomer(customerId);
            if (customer != null)
            {
                model.CustomerName = customer.Name;
            }

            model.RentingDate = DateTime.Today;
            model.ItemPopup.AvailableItemTypes = _itemService.GetItemTypes();
            var defaultValue = new ItemType() { Id = 0, Name = "<Select>" };
            model.ItemPopup.AvailableItemTypes.Insert(0, defaultValue);

            return View(model);
        }

        [HttpPost]
        public IActionResult Create(RentInfoModel model)
        {
            var rent = new Rent()
            {
                ReferenceNumber = model.ReferenceNumber,
                CustomerId = model.CustomerId,
                Description = model.Description
            };

            foreach (var rentItemModel in model.RentItems)
            {
                var rentItem = new RentItem()
                {
                    ItemId = rentItemModel.ItemId,
                    RateType = rentItemModel.RateType,
                    GivenRate = rentItemModel.GivenRate,
                    RentingDate = model.RentingDate
                };

                rent.RentItems.Add(rentItem);
            }

            rent.Payments.Add(new Payment() {
                Amount = model.AdvancePayment,
                Date = model.RentingDate,
                Description = "Advance Payment"
            });

            try
            {
                _rentService.SaveRent(rent);
                TempData["SuccessMessage"] = "Rent detail saved successfully";
                return RedirectToAction("RentList");
            }
            catch
            {
                TempData["ErrorMessage"] = "Error occured.";
                return Create(model.CustomerId, 0);
            }
        }

        public IActionResult ItemList(int searchTypeId)
        {
            var model = new List<ItemModel>();
            var items = _itemService.GetItemsWithRates(searchTypeId);
            foreach (var item in items)
            {
                var itemModel = new ItemModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    IdetificationNumber = item.IdetificationNumber,
                    ModelNumber = item.ModelNumber,
                };

                foreach (RentalRate rate in item.Rates)
                {
                    var rateModel = new RateModel()
                    {
                        RentalRateType = Enum.GetName(typeof(RentalRateType), rate.RateType),
                        Rate = rate.Rate
                    };

                    itemModel.Rates.Add(rateModel);
                }
                model.Add(itemModel);
            }
            
            return Json(model);
        }

        public IActionResult RentList(int pageNumber, int customerId, int status, string refNumber)
        {
            var pageSize = 3;
            var pageIndex = 0;
            if (pageNumber > 0)
            {
                pageIndex = pageNumber - 1;
            }

            var model = new RentPagedListModel();
            foreach (var statusId in Enum.GetValues(typeof(RentStatus)))
            {
                var rentStatusModel = new RentStatusModel() {
                    Id = (int)statusId,
                    Name = Enum.GetName(typeof(RentStatus), statusId)
                };

                model.StatusList.Add(rentStatusModel);
            }

            IPagedList<Rent> rentList = _rentService.GetRentList(customerId, refNumber, status, pageIndex, pageSize);

            foreach (var rent in rentList)
            {
                var rentModel = new RentModel() {
                    Id = rent.Id,
                    ReferenceNumber = rent.ReferenceNumber,
                    CustomerId = rent.CustomerId
                };

                model.List.Add(rentModel);
            }

            model.PagerModel = new PagerModel
            {
                PageSize = rentList.PageSize,
                TotalRecords = rentList.TotalCount,
                PageIndex = rentList.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "RentList",
                UseRouteLinks = false,
                RouteValues = new RentListRouteValues
                {
                    pageNumber = pageNumber,
                    customerId = customerId,
                    refNumber = refNumber,
                    status = status
                }
            };

            return View(model);
        }

        public IActionResult Return(int id)
        {
            var rent = _rentService.GetRentWithRentItemsAndPaymentsById(id);
            ReturnInfoModel model = null;
            if (rent != null)
            {
                model = new ReturnInfoModel()
                {
                    RentId = id,
                    ReferenceNumber = rent.ReferenceNumber,
                    ReturnDate = DateTime.Today
                };

                foreach (var rentItem in rent.RentItems)
                {
                    var returnItemModel = new ReturnItemModel()
                    {
                        Id = rentItem.Id,
                        ItemId = rentItem.ItemId,
                        RateType = rentItem.RateType,
                        GivenRate = rentItem.GivenRate,
                        RentingDate = rentItem.RentingDate
                    };

                    if (rentItem.ReturnDate != DateTime.MinValue)
                    {
                        returnItemModel.Returned = true;
                    }

                    var item = _itemService.GetItem(rentItem.ItemId);
                    if (item != null)
                    {
                        returnItemModel.ItemName = item.Name;
                    }

                    model.ReturnItems.Add(returnItemModel);
                }

                model.PreviousPayments = rent.Payments;
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult Return(ReturnInfoModel model)
        {
            var returnItemIds = new List<int>();
            foreach (var item in model.ReturnItems)
            {
                
                if (item.Returned && item.RentingDate == DateTime.MinValue)
                {
                    returnItemIds.Add(item.Id);
                }
            }

            if (returnItemIds.Count() > 0)
            {
                try
                {
                    _rentService.ReturnRentItems(model.RentId, model.ReturnDate, returnItemIds);
                    TempData["SuccessMessage"] = "Saved successfully";
                    return RedirectToAction("ReturnInvoice", new { rentId = model.RentId });
                }
                catch
                {
                    TempData["ErrorMessage"] = "Error occured.";
                    return View(model);
                }
            }
            else
            {
                return Return(model.RentId);
            }
        }

        public IActionResult ReturnInvoice(int rentId)
        {
            var invoiceInfo = _rentService.GetReturnInvoiceInfo(rentId);
            var model = new ReturnInvoiceModel()
            {
                ReferenceNumber = invoiceInfo.ReferenceNumber,
                CustomerId = invoiceInfo.CustomerId,
                CustomerName = invoiceInfo.CustomerName,
                Items = invoiceInfo.Items
            };

            model.TotalAmount = invoiceInfo.Items.Sum(i => i.Item.GivenRate);
            model.PaidAmount = invoiceInfo.Payments.Sum(i => i.Amount);
            model.BalanceAmount = model.TotalAmount - model.PaidAmount;

            return View(model);
        }
    }
}