﻿using Microsoft.AspNetCore.Mvc;
using RentingManager.Client.Models.Common;
using RentingManager.Client.Models.Item;
using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using RentingManager.Server.Services;
using System;
using System.Collections.Generic;

namespace RentingManager.Client.Controllers
{
    public class ItemController : Controller
    {
        IItemService _itemService;

        public ItemController(IItemService itemService)
        {
            _itemService = itemService;
        }

        public IActionResult CreateType()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateType(ItemTypeModel model)
        {
            ItemType type = new ItemType()
            {
                Name = model.Name.Trim(),
                Description = model.Description.Trim()
            };

            try
            {
                _itemService.SaveItemType(type);
                TempData["SuccessMessage"] = "Item type saved successfully";
                return RedirectToAction("ItemTypeList");
            }
            catch
            {
                TempData["ErrorMessage"] = "Error occured.";
                return View();
            }
        }

        public ActionResult EditType(int id)
        {
            IList<ItemTypeModel> model = new List<ItemTypeModel>();
            var itemType = _itemService.GetItemType(id);
            var itemTypeModel = new ItemTypeModel()
            {
                Id = itemType.Id,
                Name = itemType.Name,
                Description = itemType.Description
            };

            return View(itemTypeModel);
        }

        [HttpPost]
        public ActionResult EditType(ItemTypeModel model)
        {
            var itemType = new ItemType()
            {
                Id = model.Id,
                Name = model.Name.Trim(),
                Description = model.Description.Trim()
            };

            try
            {
                _itemService.UpdateItemtype(itemType);
                TempData["SuccessMessage"] = "Item type updated successfully";
                return RedirectToAction("ItemTypeList");

            }
            catch
            {
                TempData["ErrorMessage"] = "Error occured.";
            }

            return View();
        }

        public ActionResult DeleteType(int id)
        {
            try
            {
                _itemService.DeleteItemType(id);
                TempData["SuccessMessage"] = "Item type deleted successfully";
                return RedirectToAction("ItemTypeList");

            }
            catch
            {
                TempData["ErrorMessage"] = "Error occured.";
                return RedirectToAction("ItemTypeList");
            }
        }

        public IActionResult ItemTypeList(int pageNumber, string searchByNameString)
        {
            var pageSize = 2;
            var pageIndex = 0;
            if (pageNumber > 0)
            {
                pageIndex = pageNumber - 1;
            }

            ItemTypePagedListModel model = new ItemTypePagedListModel();
            IPagedList<ItemType> itemTypes = _itemService.GetItemTypes(searchByNameString?.Trim(), pageIndex, pageSize);
            foreach (var itemType in itemTypes)
            {
                ItemTypeModel itemTypeModel = new ItemTypeModel()
                {
                    Id = itemType.Id,
                    Name = itemType.Name,
                    Description = itemType.Description
                };

                model.List.Add(itemTypeModel);
            }

            model.PagerModel = new PagerModel
            {
                PageSize = itemTypes.PageSize,
                TotalRecords = itemTypes.TotalCount,
                PageIndex = itemTypes.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "ItemTypeList",
                UseRouteLinks = false,
                RouteValues = new ItemTypeListRouteValues
                {
                    pageNumber = pageNumber,
                    searchByNameString = searchByNameString
                }
            };

            return View(model);
        }

        public IActionResult CreateItem()
        {
            var model = new ItemInfoModel();
            foreach (var name in Enum.GetNames(typeof(RentalRateType)))
            {
                model.Rates.Add(new RentalRateModel() { RentalRateType = name, DisplayLabel = string.Format("{0} Rate", name) });
            }

            model.AvailableItemTypes = _itemService.GetItemTypes(string.Empty);
            var defaultValue = new ItemType() { Id = 0, Name = "<Select>" };
            model.AvailableItemTypes.Insert(0, defaultValue);

            return View(model);
        }

        [HttpPost]
        public IActionResult CreateItem(ItemInfoModel model)
        {
            var item = new Item();
            item.Name = model.Name;
            item.ModelNumber = model.ModelNumber;
            item.IdetificationNumber = model.IdetificationNumber;
            item.TypeId = model.SelectedItemTypeId;
            item.Description = model.Description;
            foreach (var rateModel in model.Rates)
            {
                var rate = new RentalRate()
                {
                    RateType = Enum.Parse<RentalRateType>(rateModel.RentalRateType),
                    Rate = rateModel.Rate
                };

                item.Rates.Add(rate);
            }

            try
            {
                _itemService.SaveItem(item);
                TempData["SuccessMessage"] = "Item saved successfully";
                return RedirectToAction("ItemList");
            }
            catch
            {
                TempData["ErrorMessage"] = "Error occured.";
                return CreateItem();
            }
        }

        public IActionResult ItemList(int pageNumber, int searchTypeId, string searchName, string searchIdentification, string searchModelNumber)
        {
            var pageSize = 3;
            var pageIndex = 0;
            if (pageNumber > 0)
            {
                pageIndex = pageNumber - 1;
            }

            IPagedList<Item> items = _itemService.GetItems(searchTypeId, searchName, searchIdentification, searchModelNumber, pageIndex, pageSize);
            ItemPagedListModel model = new ItemPagedListModel();
            foreach (var item in items)
            {
                var itemModel = new ItemModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    IdetificationNumber = item.IdetificationNumber,
                    ModelNumber = item.ModelNumber
                };

                model.List.Add(itemModel);
            }

            if (model.AvailableItemTypes == null)
            {
                model.AvailableItemTypes = _itemService.GetItemTypes();
                var defaultValue = new ItemType() { Id = 0, Name = "<Select>" };
                model.AvailableItemTypes.Insert(0, defaultValue);
            }

            model.PagerModel = new PagerModel
            {
                PageSize = items.PageSize,
                TotalRecords = items.TotalCount,
                PageIndex = items.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "ItemList",
                UseRouteLinks = false,
                RouteValues = new ItemListRouteValues
                {
                    searchTypeId = searchTypeId,
                    pageNumber = pageNumber,
                    searchName = searchName,
                    searchModelNumber = searchModelNumber
                }
            };

            return View(model);
        }

        public ActionResult EditItem(int id)
        {
            var item = _itemService.GetItemsWithRatesById(id);
            var model = new ItemInfoModel();
            if (item != null)
            {
                model.Id = item.Id;
                model.SelectedItemTypeId = item.TypeId;
                model.Name = item.Name;
                model.IdetificationNumber = item.IdetificationNumber;
                model.ModelNumber = item.ModelNumber;
                model.Description = item.Description;

                model.AvailableItemTypes = _itemService.GetItemTypes(string.Empty);
                var defaultValue = new ItemType() { Id = 0, Name = "<Select>" };
                model.AvailableItemTypes.Insert(0, defaultValue);

                string rateTypeName;
                foreach (RentalRate rate in item.Rates)
                {
                    rateTypeName = Enum.GetName(typeof(RentalRateType), rate.RateType);
                    var rateModel = new RentalRateModel()
                    {
                        Id = rate.Id,
                        RentalRateType = rateTypeName,
                        Rate = rate.Rate,
                        DisplayLabel = string.Format("{0} Rate", rateTypeName)
                    };

                    model.Rates.Add(rateModel);
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult EditItem(ItemInfoModel model)
        {
            var item = new Item
            {
                Id = model.Id,
                Name = model.Name,
                ModelNumber = model.ModelNumber,
                IdetificationNumber = model.IdetificationNumber,
                TypeId = model.SelectedItemTypeId,
                Description = model.Description
            };

            foreach (var rateModel in model.Rates)
            {
                var rate = new RentalRate()
                {
                    Id = rateModel.Id,
                    ItemId = model.Id,
                    RateType = Enum.Parse<RentalRateType>(rateModel.RentalRateType),
                    Rate = rateModel.Rate
                };

                item.Rates.Add(rate);
            }

            try
            {
                _itemService.UpdateItem(item);
                TempData["SuccessMessage"] = "Item updated successfully";
                return RedirectToAction("ItemList");
            }
            catch
            {
                TempData["ErrorMessage"] = "Error occured.";
                return EditItem(model.Id);
            }
        }

        public ActionResult DeleteItem(int id)
        {
            try
            {
                _itemService.DeleteItem(id);
                TempData["SuccessMessage"] = "Item deleted successfully";
                return RedirectToAction("ItemList");

            }
            catch
            {
                TempData["ErrorMessage"] = "Error occured.";
                return RedirectToAction("ItemList");
            }
        }
    }
}