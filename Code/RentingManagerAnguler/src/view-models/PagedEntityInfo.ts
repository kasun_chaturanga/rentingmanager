

export class PagedEntityInfo<T> {
  public entities: T[];
  public totalCount: number;
}
