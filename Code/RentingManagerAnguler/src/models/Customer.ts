export class Customer {
  public id: number;
  public name: string;
  public nic: string;
  public addressLine1: string;
  public addressLine2: string;
  public addressLine3: string;
}
