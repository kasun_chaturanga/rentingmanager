import { Component, OnInit } from '@angular/core';
import { ItemType } from '../../models/ItemType';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppSettingsService } from '../shared/AppSettingsService';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-item-type',
  templateUrl: './item-type.component.html',
  styleUrls: ['./item-type.component.css']
})
export class ItemTypeComponent implements OnInit {
  itemType: ItemType = new ItemType();

  constructor(private httpClient: HttpClient,
    private appSettingsService: AppSettingsService,
    private toastr: ToastrService,
    private router: Router,
    private routeParams: ActivatedRoute)
  {
  }

  ngOnInit() {
    this.itemType.id = this.routeParams.snapshot.params['id'];
    if (this.itemType.id > 0) {
      this.getDataAndDisplay()
    }
  }

  onSubmit() {
    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = {
      headers: httpHeaders
    };

    var url = this.appSettingsService.getDefaultUrl() + "/itemtypes";
    if (this.itemType.id > 0) {
      this.httpClient.put<ItemType>(url, this.itemType, options).subscribe(null,
        error => {
          console.error(error);
          this.toastr.error("Error occured.");
        },
        () => {
          this.router.navigate(['/item-type-list']);
          this.toastr.success("Item type updated successfully.")
        });
    }
    else {
      this.httpClient.post<ItemType>(url, this.itemType, options).subscribe(null,
        error => {
          console.error(error);
          this.toastr.error("Error occured.");
        },
        () => {
          this.router.navigate(['/item-type-list']);
          this.toastr.success("Item type saved successfully.")
        });
    }
  }

  getDataAndDisplay() {
    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = {
      headers: httpHeaders
    };

    var url = this.appSettingsService.getDefaultUrl() + "/itemtypes/" + this.itemType.id;
    this.httpClient.get(url, options).subscribe(result => {
      this.itemType = result as ItemType;
    });
  }
}
