import { Component, OnInit } from '@angular/core';
import { ItemTypeSearchModel } from '../../view-models/ItemTypeSearchModel';
import { PagedEntityInfo } from '../../view-models/PagedEntityInfo';
import { ItemType } from '../../models/ItemType';
import { AppSettingsService } from '../shared/AppSettingsService';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { SimpleModalService } from 'ngx-simple-modal';
import { ConfirmComponent } from '../confirm/confirm.component';

@Component({
  selector: 'app-item-type-list',
  templateUrl: './item-type-list.component.html',
  styleUrls: ['./item-type-list.component.css']
})
export class ItemTypeListComponent implements OnInit {
  itemTypeInfo: PagedEntityInfo<ItemType>;
  searchModel: ItemTypeSearchModel = new ItemTypeSearchModel();
  pageSize: number;
  page: number;

  constructor(private appSettingsService: AppSettingsService,
    private httpClient: HttpClient,
    private toastr: ToastrService,
    private simpleModalService: SimpleModalService) { }

  ngOnInit() {
    this.pageSize = this.appSettingsService.getPageSize();
    this.searchModel = new ItemTypeSearchModel();
    this.searchModel.name = "";
    this.getItemTypeList(1);
  }

  getItemTypeList(pageNumber: number) {
    let httpParams = {
      name: this.searchModel.name,
      pageIndex: (pageNumber - 1).toString(),
      pageSize: this.pageSize.toString()
    }

    let options = {
      params: httpParams
    };

    var url = this.appSettingsService.getDefaultUrl() + "/itemTypes";
    this.httpClient.get(url, options).subscribe(result => {
      this.itemTypeInfo = result as PagedEntityInfo<ItemType>;
    },
      error => {
        console.error(error);
        this.toastr.error("Error occured.");
      });
  }

  search() {
    this.getItemTypeList(1);
  }

  loadPage(page: number) {
    this.getItemTypeList(page);
  }

  delete(itemTypeId: number) {
    var url = this.appSettingsService.getDefaultUrl() + "/itemtypes/" + itemTypeId;
    this.httpClient.delete(url).subscribe(null,
      error => {
        console.error(error);
        this.toastr.error("Error occured.");
      },
      () => {
        this.getItemTypeList(1);
        this.toastr.success("Item type deleted.");
      });
  }

  deleteConfirm(itemTypeId: number) {
    this.simpleModalService.addModal(ConfirmComponent, {
      title: 'Confirmation',
      message: 'Are you sure you wish to delete this item type?'
    })
      .subscribe((isConfirmed) => {
        if (isConfirmed) {
          this.delete(itemTypeId);
        }
      });
  }
}
