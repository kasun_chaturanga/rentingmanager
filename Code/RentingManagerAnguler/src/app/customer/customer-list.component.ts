import { Component, OnInit } from '@angular/core';
import { PagedEntityInfo } from '../../view-models/PagedEntityInfo';
import { HttpClient} from '@angular/common/http';
import { Customer } from '../../models/Customer';
import { AppSettingsService } from '../shared/AppSettingsService';
import { CustomerSearchModel } from '../../view-models/CustomerSearchModel';
import { ToastrService } from 'ngx-toastr';
import { ConfirmComponent } from '../confirm/confirm.component';
import { SimpleModalService } from 'ngx-simple-modal';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  searchModel: CustomerSearchModel;
  customerInfo: PagedEntityInfo<Customer>;
  pageSize: number;
  page: number;

  constructor(private httpClient: HttpClient,
    private appSettingsService: AppSettingsService,
    private toastr: ToastrService,
    private simpleModalService: SimpleModalService
  )
  { 
  }

  ngOnInit() {
    this.pageSize = this.appSettingsService.getPageSize();
    this.searchModel = new CustomerSearchModel();
    this.searchModel.nic = "";
    this.searchModel.name = "";
    this.getCustomerList(1);
  }

  search() {
    this.page = 1;
    this.getCustomerList(1);
  }

  deleteConfirm(customerId: number) {
    this.simpleModalService.addModal(ConfirmComponent, {
      title: 'Confirmation',
      message: 'Are you sure you wish to delete this customer?'
    })
      .subscribe((isConfirmed) => {
        if (isConfirmed) {
          this.delete(customerId);
        }
      });
  }

  delete(customerId: number) {
    var url = this.appSettingsService.getDefaultUrl() + "/customers/" + customerId;
    this.httpClient.delete(url).subscribe(null,
      error => {
        console.error(error);
        this.toastr.error("Error occured.");
      },
      () => {
      this.getCustomerList(1);
      this.toastr.success("Customer deleted.");
      });
  }

  getCustomerList(pageNumber: number) {
    let httpParams = {
      nic: this.searchModel.nic,
      name: this.searchModel.name,
      pageIndex: (pageNumber - 1).toString(),
      pageSize: this.pageSize.toString()
    }
    
    let options = {
      params: httpParams
    };

    var url = this.appSettingsService.getDefaultUrl() + "/customers";
    this.httpClient.get(url, options).subscribe(result => {
      this.customerInfo = result as PagedEntityInfo<Customer>;
    },
      error => {
      console.error(error);
      this.toastr.error("Error occured.");
    });
  }

  loadPage(page: number) {
    this.getCustomerList(page);
  }
}
