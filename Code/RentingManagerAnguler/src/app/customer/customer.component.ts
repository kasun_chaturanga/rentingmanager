import { Component, OnInit } from '@angular/core';
import { Customer } from '../../models/Customer';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { AppSettingsService } from '../shared/AppSettingsService';
import { ToastrService } from 'ngx-toastr'; 

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  customer: Customer = new Customer();

  constructor(private httpClient: HttpClient,
    private routeParams: ActivatedRoute,
    private appSettingsService: AppSettingsService,
    private router: Router,
    private toastr: ToastrService
  )
  {
    
  }

  ngOnInit() {
    this.customer.id = this.routeParams.snapshot.params['id'];
    if (this.customer.id > 0) {
      this.getDataAndDisplay()
    }
  }

  onSubmit() {
    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = {
      headers: httpHeaders
    }; 
    
    var url = this.appSettingsService.getDefaultUrl() + "/customers";
    if (this.customer.id > 0) {
      this.httpClient.put<Customer>(url, this.customer, options).subscribe(null,
        error => {
          console.error(error);
          this.toastr.error("Error occured.");
        },
        () => {
          this.router.navigate(['/customer-list']);
          this.toastr.success("Customer updated successfully.")
        });
    }
    else {
      this.httpClient.post<Customer>(url, this.customer, options).subscribe(null,
        error => {
          console.error(error);
          this.toastr.error("Error occured.");
        },
        () => {
          this.router.navigate(['/customer-list']);
          this.toastr.success("Customer saved successfully.")
        });
    }
  }

  getDataAndDisplay() {
    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = {
      headers: httpHeaders
    };

    var url = this.appSettingsService.getDefaultUrl() + "/customers/" + this.customer.id;
    this.httpClient.get(url, options).subscribe(result => {
      this.customer = result as Customer;
    });
  }
}
