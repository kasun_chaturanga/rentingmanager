import { Injectable } from '@angular/core';
import { AppSettings } from "./appsettings";

@Injectable()
export class AppSettingsService {
  getDefaultUrl(): string {
    return AppSettings.defaultUrl;
  }

  getPageSize(): number {
    return AppSettings.pageSize;
  }
}
