import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { CustomerComponent } from './customer/customer.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CustomerListComponent } from './customer/customer-list.component';
import { AppSettingsService } from './shared/AppSettingsService';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { ItemTypeComponent } from './item-type/item-type.component';
import { ItemTypeListComponent } from './item-type/item-type-list.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { SimpleModalService, SimpleModalModule } from 'ngx-simple-modal';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    CustomerComponent,
    CustomerListComponent,
    ItemTypeComponent,
    ItemTypeListComponent,
    ConfirmComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    SimpleModalModule.forRoot({ container: "modal-container" }),
    RouterModule.forRoot([
      { path: 'customer/:id', component: CustomerComponent },
      { path: 'customer-list', component: CustomerListComponent },
      { path: 'item-type/:id', component: ItemTypeComponent },
      { path: 'item-type-list', component: ItemTypeListComponent }
    ])
  ],
  entryComponents: [
    ConfirmComponent  
  ],
  providers: [AppSettingsService, SimpleModalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
