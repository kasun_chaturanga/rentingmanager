﻿using Microsoft.AspNetCore.Mvc;
using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using RentingManager.Server.Services;
using RentingManager.Server.WebApi.Core;

namespace RentingManager.Server.WebApi.Controllers
{
    [Route("ItemTypes")]
    [ApiController]
    public class ItemTypeController : ControllerBase
    {
        private IItemService _itemSevice;

        public ItemTypeController(IItemService itemSevice)
        {
            _itemSevice = itemSevice;
        }

        public void Post([FromBody]ItemType itemType)
        {
            _itemSevice.SaveItemType(itemType);
        }

        [HttpGet("{id}")]
        public ItemType Get(int id)
        {
            return _itemSevice.GetItemType(id);
        }

        [HttpGet]
        public IPagedEntityInfo<ItemType> Get(string name, int pageIndex, int pageSize)
        {
            IPagedList<ItemType> pagedList = _itemSevice.GetItemTypes(name, pageIndex, pageSize);
            IPagedEntityInfo<ItemType> info = new PagedEntityInfo<ItemType>(pagedList);

            return info;
        }

        [HttpPut]
        public void Put([FromBody]ItemType itemType)
        {
            _itemSevice.UpdateItemtype(itemType);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _itemSevice.DeleteItemType(id);
        }
    }
}