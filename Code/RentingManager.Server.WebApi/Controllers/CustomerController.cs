﻿using Microsoft.AspNetCore.Mvc;
using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using RentingManager.Server.Services;
using RentingManager.Server.WebApi.Core;

namespace RentingManager.Server.WebApi.Controllers
{
    [Route("Customers")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpPost]
        public void Post([FromBody] Customer customer)
        {
            _customerService.SaveCustomer(customer);
        }

        [HttpGet("{id}")]
        public Customer Get(int id)
        {
            return _customerService.GetCustomer(id);
        }

        [HttpGet]
        public IPagedEntityInfo<Customer> Get(string nic, string name, int pageIndex, int pageSize)
        {
            IPagedList<Customer> pagedList = _customerService.GetCustomers(nic, name, pageIndex, pageSize);
            IPagedEntityInfo<Customer> info = new PagedEntityInfo<Customer>(pagedList);

            return info;
        }

        [HttpPut]
        public void Put([FromBody]Customer customer)
        {
            _customerService.UpdateCustomer(customer);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _customerService.DeleteCustomer(id);
        }
    }
}