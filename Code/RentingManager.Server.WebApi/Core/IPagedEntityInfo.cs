﻿using System.Collections.Generic;

namespace RentingManager.Server.WebApi.Core
{
    public interface IPagedEntityInfo<T>
    {
        List<T> Entities { get; set; }

        int TotalCount { get; set; }
    }
}
