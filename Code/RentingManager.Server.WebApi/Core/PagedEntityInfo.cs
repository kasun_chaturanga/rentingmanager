﻿using RentingManager.Server.Core;
using System.Collections.Generic;

namespace RentingManager.Server.WebApi.Core
{
    public class PagedEntityInfo<T> : IPagedEntityInfo<T>
    {
        public PagedEntityInfo(IPagedList<T> pagedList)
        {
            Entities = new List<T>();
            Entities.AddRange(pagedList);
            TotalCount = pagedList.TotalCount;
        }

        public List<T> Entities { get; set; }

        public int TotalCount { get; set; }
    }
}
