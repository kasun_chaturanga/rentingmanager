﻿using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using RentingManager.Server.Data;
using RentingManager.Server.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RentingManager.Server.Services
{
    public class RentService : IRentService
    {
        private IUnitOfWork _unitOfWork;

        public RentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void SaveRent(Rent rent)
        {
            _unitOfWork.Rents.Add(rent);
            _unitOfWork.Complete();
        }

        public Rent GetRent(int id)
        {
            return _unitOfWork.Rents.Get(id);
        }

        public Rent GetRentWithRentItemsAndPaymentsById(int id)
        {
            return _unitOfWork.Rents.GetWithRentItemsAndPaymentsById(id);
        }

        public Rent GetRentWithRentItemsById(int id)
        {
            return _unitOfWork.Rents.GetWithRentItemsById(id);
        }

        public void UpdateRent(Rent rent)
        {
            _unitOfWork.Rents.Update(rent);
            _unitOfWork.Complete();
        }

        public IPagedList<Rent> GetRentList(int customerId, string referenceNumber, int status, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _unitOfWork.Rents.GetPagedList(customerId, referenceNumber, status, pageIndex, pageSize);
        }

        public void ReturnRentItems(int rentId, DateTime returnDate, IList<int> returnItemIds)
        {
            if (returnItemIds != null && returnItemIds.Count() > 0)
            {
                bool nonReturnItemsExist = false;
                var existingRent = _unitOfWork.Rents.Get(rentId);
                if (existingRent != null)
                {
                    foreach (var item in existingRent.RentItems)
                    {
                        if (returnItemIds.Contains(item.Id))
                        {
                            item.ReturnDate = returnDate;
                        }
                        else if (item.ReturnDate == DateTime.MinValue)
                        {
                            nonReturnItemsExist = true;
                        }
                    }

                    if (!nonReturnItemsExist)
                    {
                        existingRent.Status = RentStatus.Returned;
                    }

                    UpdateRent(existingRent);
                }
            }
        }

        public ReturnInvoiceInfo GetReturnInvoiceInfo(int rentId)
        {
            var rent = _unitOfWork.Rents.GetWithRentItemsAndPaymentsById(rentId);
            var info = new ReturnInvoiceInfo
            {
                ReferenceNumber = rent.ReferenceNumber,
                CustomerId = rent.CustomerId,
                Payments = rent.Payments
            };

            var customer = _unitOfWork.Customers.Get(rent.CustomerId);
            if (customer != null)
            {
                info.CustomerName = customer.Name;
            }

            foreach (var item in rent.RentItems)
            {
                var invoiceItem = new InvoiceItem()
                {
                    Item = item,
                    RentAmount = RentCalculationHelper.CalculateRent(item.RentingDate, item.ReturnDate, item.RateType, item.GivenRate)
                };

                info.Items.Add(invoiceItem);
            }

            return info;
        }
    }
}
