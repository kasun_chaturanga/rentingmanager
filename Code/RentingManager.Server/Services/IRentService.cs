﻿using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using System;
using System.Collections.Generic;

namespace RentingManager.Server.Services
{
    public interface IRentService
    {
        void SaveRent(Rent rent);

        Rent GetRent(int id);

        Rent GetRentWithRentItemsAndPaymentsById(int id);

        Rent GetRentWithRentItemsById(int id);

        void UpdateRent(Rent rent);

        IPagedList<Rent> GetRentList(int customerId, string referenceNumber, int status, int pageIndex = 0, int pageSize = int.MaxValue);

        void ReturnRentItems(int rentId, DateTime returnDate, IList<int> returnItemIds);

        ReturnInvoiceInfo GetReturnInvoiceInfo(int rentId);
    }
}
