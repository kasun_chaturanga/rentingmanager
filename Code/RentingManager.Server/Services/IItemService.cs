﻿using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using System.Collections.Generic;

namespace RentingManager.Server.Services
{
    public interface IItemService
    {
        void SaveItemType(ItemType type);

        ItemType GetItemType(int id);

        void UpdateItemtype(ItemType itemType);

        void DeleteItemType(int id);

        IPagedList<ItemType> GetItemTypes(string name = "", int pageIndex = 0, int pageSize = int.MaxValue);

        void SaveItem(Item item);

        Item GetItem(int id);

        Item GetItemsWithRatesById(int id);

        void UpdateItem(Item item);

        void DeleteItem(int id);

        IPagedList<Item> GetItems(int typeId, string name, string identificationNumber, string modelNumber, int pageIndex = 0, int pageSize = int.MaxValue);

        IList<Item> GetItemsWithRates(int typeId);
    }
}
