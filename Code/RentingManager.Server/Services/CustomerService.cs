﻿using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using RentingManager.Server.Data;

namespace RentingManager.Server.Services
{
    public class CustomerService : ICustomerService
    {
        private IUnitOfWork _unitOfWork;

        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Customer GetCustomer(int id)
        {
            return _unitOfWork.Customers.Get(id);
        }

        public void SaveCustomer(Customer customer)
        {
            _unitOfWork.Customers.Add(customer);
            _unitOfWork.Complete();
        }

        public void UpdateCustomer(Customer customer)
        {
            _unitOfWork.Customers.Update(customer);
            _unitOfWork.Complete();
        }

        public void DeleteCustomer(int id)
        {
            Customer customer = _unitOfWork.Customers.Get(id);
            if (customer != null)
            {
                _unitOfWork.Customers.Remove(customer);
                _unitOfWork.Complete();
            }
        }

        public IPagedList<Customer> GetCustomers(string nic, string name, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _unitOfWork.Customers.GetCustomers(nic, name, pageIndex, pageSize);
        }
    }
}
