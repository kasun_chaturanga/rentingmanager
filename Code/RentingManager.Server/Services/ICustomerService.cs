﻿using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace RentingManager.Server.Services
{
    public interface ICustomerService
    {
        Customer GetCustomer(int id);

        void SaveCustomer(Customer customer);

        void UpdateCustomer(Customer customer);

        void DeleteCustomer(int id);

        IPagedList<Customer> GetCustomers(string nic, string name, int pageIndex = 0, int pageSize = int.MaxValue);
    }
}
