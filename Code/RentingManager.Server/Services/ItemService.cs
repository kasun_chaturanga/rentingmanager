﻿using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using RentingManager.Server.Data;
using System.Collections.Generic;

namespace RentingManager.Server.Services
{
    public class ItemService : IItemService
    {
        private IUnitOfWork _unitOfWork;

        public ItemService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void SaveItemType(ItemType type)
        {
            _unitOfWork.ItemTypes.Add(type);
            _unitOfWork.Complete();        }

        public ItemType GetItemType(int id)
        {
            return _unitOfWork.ItemTypes.Get(id);
        }

        public void UpdateItemtype(ItemType itemType)
        {
            _unitOfWork.ItemTypes.Update(itemType);
            _unitOfWork.Complete();
        }

        public void DeleteItemType(int id)
        {
            var itemType = _unitOfWork.ItemTypes.Get(id);
            if (itemType != null)
            {
                _unitOfWork.ItemTypes.Remove(itemType);
                _unitOfWork.Complete();
            }
        }

        public IPagedList<ItemType> GetItemTypes(string name = "", int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _unitOfWork.ItemTypes.GetPagedList(name, pageIndex, pageSize);
        }

        public void SaveItem(Item item)
        {
            _unitOfWork.Items.Add(item);
            _unitOfWork.Complete();
        }

        public Item GetItem(int id)
        {
            return _unitOfWork.Items.Get(id);
        }

        public Item GetItemsWithRatesById(int id)
        {
            return _unitOfWork.Items.GetWithRatesById(id);
        }

        public void UpdateItem(Item item)
        {
            _unitOfWork.Items.Update(item);
            _unitOfWork.Complete();
        }

        public void DeleteItem(int id)
        {
            var item = _unitOfWork.Items.Get(id);
            if (item != null)
            {
                _unitOfWork.Items.Remove(item);
                _unitOfWork.Complete();
            }
        }

        public IPagedList<Item> GetItems(int typeId, string name, string identificationNumber, string modelNumber, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            return _unitOfWork.Items.GetPagedList(typeId, name, identificationNumber, modelNumber, pageIndex, pageSize);
        }

        public IList<Item> GetItemsWithRates(int typeId)
        {
            return _unitOfWork.Items.GetWithRatesByTypeId(typeId);
        }
    }
}
