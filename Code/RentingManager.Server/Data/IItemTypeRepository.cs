﻿using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;

namespace RentingManager.Server.Data
{
    public interface IItemTypeRepository : IRepository<ItemType>
    {
        PagedList<ItemType> GetPagedList(string name, int pageIndex, int pageSize);
    }
}
