﻿using Microsoft.EntityFrameworkCore;
using RentingManager.Server.Core.Domain;

namespace RentingManager.Server.Data
{
    public class RentingDbContext : DbContext
    {
        public RentingDbContext() : base()
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=RentingManagerDb;Integrated Security=SSPI");
        }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<ItemType> ItemTypes { get; set; }

        public DbSet<RentalRate> RentalRates { get; set; }

        public DbSet<Item> Items { get; set; }

        public DbSet<Rent> Rents { get; set; }

        public DbSet<RentItem> RentItems { get; set; }

        public DbSet<Payment> Payments { get; set; }
    }
}
