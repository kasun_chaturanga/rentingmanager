﻿using RentingManager.Server.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace RentingManager.Server.Data
{
    public interface IUnitOfWork : IDisposable
    {
        ICustomerRepository Customers { get; }
        IItemTypeRepository ItemTypes { get; }
        IItemRepository Items { get; }
        IRentRepository Rents { get; }
        int Complete();
    }
}
