﻿using RentingManager.Server.Core.Domain;

namespace RentingManager.Server.Data
{
    public interface IRepository<TEntity> where TEntity : EntityBase
    {
        void Add(TEntity entity);
        TEntity Get(int id);
        void Update(TEntity entity);
        void Remove(TEntity entity);
    }
}
