﻿using Microsoft.EntityFrameworkCore;
using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using System.Linq;

namespace RentingManager.Server.Data
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(RentingDbContext context) : base(context)
        {
            
        }

        public IPagedList<Customer> GetCustomers(string nic, string name, int pageIndex, int pageSize)
        {
            IPagedList<Customer> customers = new PagedList<Customer>(
                Entities.OrderBy(c => c.Name)
                .Where(c => string.IsNullOrEmpty(nic) || c.Nic.StartsWith(nic))
                .Where(c => string.IsNullOrEmpty(name) || c.Name.StartsWith(name))
                , pageIndex, pageSize);

            return customers;
        }
    }
}
