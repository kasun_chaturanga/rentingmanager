﻿using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using System.Collections.Generic;

namespace RentingManager.Server.Data
{
    public interface IItemRepository : IRepository<Item>
    {
        Item GetWithRatesById(int id);

        IPagedList<Item> GetPagedList(int typeId, string name, string identificationNumber, string modelNumber, int pageIndex, int pageSize);

        IList<Item> GetWithRatesByTypeId(int typeId);
    }
}
