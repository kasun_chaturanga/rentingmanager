﻿using Microsoft.EntityFrameworkCore;
using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using System.Linq;

namespace RentingManager.Server.Data
{
    public class RentRepository : Repository<Rent>, IRentRepository
    {
        public RentRepository(RentingDbContext dbContext) : base(dbContext)
        {

        }

        public Rent GetWithRentItemsAndPaymentsById(int id)
        {
            return Entities.Where(r => r.Id == id).Include(r => r.RentItems).Include(r => r.Payments).SingleOrDefault();
        }

        public Rent GetWithRentItemsById(int id)
        {
            return Entities.Where(r => r.Id == id).Include(r => r.RentItems).SingleOrDefault();
        }

        public IPagedList<Rent> GetPagedList(int customerId, string referenceNumber, int status, int pageIndex, int pageSize)
        {
            IQueryable<Rent> source = Entities.Where(r => (customerId == 0 || r.CustomerId == customerId) 
                    && (string.IsNullOrEmpty(referenceNumber) || r.ReferenceNumber == referenceNumber) 
                    && (status == 0 || (int)r.Status == status));

            return new PagedList<Rent>(source, pageIndex, pageSize);
        }
    }
}
