﻿using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using System.Linq;

namespace RentingManager.Server.Data
{
    public class ItemTypeRepository :Repository<ItemType>, IItemTypeRepository
    {
        public ItemTypeRepository(RentingDbContext context) : base(context)
        {
            
        }

        public PagedList<ItemType> GetPagedList(string name, int pageIndex, int pageSize)
        {
            IQueryable<ItemType> query = Entities.OrderBy(t => t.Name)
                .Where(t => string.IsNullOrEmpty(name) || t.Name.StartsWith(name));

            return new PagedList<ItemType>(query, pageIndex, pageSize);
        }
    }
}
