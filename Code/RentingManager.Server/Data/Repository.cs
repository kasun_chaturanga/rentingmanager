﻿using Microsoft.EntityFrameworkCore;
using RentingManager.Server.Core.Domain;

namespace RentingManager.Server.Data
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : EntityBase
    {
        private DbSet<TEntity> _entities;

        private DbContext _dbContext;

        public Repository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Gets an entity set
        /// </summary>
        protected virtual DbSet<TEntity> Entities
        {
            get
            {
                if (_entities == null)
                    _entities = _dbContext.Set<TEntity>();

                return _entities;
            }
        }

        public void Add(TEntity entity)
        {
            Entities.Add(entity);
        }

        public TEntity Get(int id)
        {
            return Entities.Find(id);
        }

        public void Update(TEntity entity)
        {
            Entities.Update(entity);
        }

        public void Remove(TEntity entity)
        {
            Entities.Remove(entity);
        }
    }
}
