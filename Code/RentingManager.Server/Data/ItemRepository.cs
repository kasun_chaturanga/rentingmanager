﻿using Microsoft.EntityFrameworkCore;
using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;
using System.Collections.Generic;
using System.Linq;

namespace RentingManager.Server.Data
{
    public class ItemRepository : Repository<Item>, IItemRepository
    {
        public ItemRepository(RentingDbContext dbContext) : base(dbContext)
        {
            
        }

        public Item GetWithRatesById(int id)
        {
            return Entities.Where(i => i.Id == id).Include(i => i.Rates).SingleOrDefault();
        }

        public IPagedList<Item> GetPagedList(int typeId, string name, string identificationNumber, string modelNumber,int pageIndex, int pageSize)
        {
            IQueryable<Item> query = Entities.OrderBy(i => i.Name)
                .Where(i => (typeId == 0 || i.TypeId == typeId)
                && (string.IsNullOrEmpty(name) || i.Name.StartsWith(name))
                && (string.IsNullOrEmpty(identificationNumber) || i.IdetificationNumber.StartsWith(identificationNumber))
                && (string.IsNullOrEmpty(modelNumber) || i.ModelNumber.StartsWith(modelNumber)));

            return new PagedList<Item>(query, pageIndex, pageSize);
        }

        public IList<Item> GetWithRatesByTypeId(int typeId)
        {
            return Entities.OrderBy(i => i.Name)
                .Where(i => (typeId == 0 || i.TypeId == typeId)).Include(i => i.Rates).ToList();
        }
    }
}
