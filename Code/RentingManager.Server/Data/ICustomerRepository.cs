﻿using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;

namespace RentingManager.Server.Data
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        IPagedList<Customer> GetCustomers(string nic, string name, int pageIndex, int pageSize);
    }
}
