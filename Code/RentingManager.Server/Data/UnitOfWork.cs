﻿namespace RentingManager.Server.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly RentingDbContext _dbContext;

        public UnitOfWork(RentingDbContext dbContext, ICustomerRepository customers, IItemTypeRepository itemTypes, IItemRepository items, IRentRepository rents)
        {
            _dbContext = dbContext;
            Customers = customers;
            ItemTypes = itemTypes;
            Items = items;
            Rents = rents;
        }

        public ICustomerRepository Customers { get; private set; }

        public IItemTypeRepository ItemTypes { get; private set; }

        public IItemRepository Items { get; private set; }

        public IRentRepository Rents { get; private set; }

        public int Complete()
        {
            return _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
