﻿using RentingManager.Server.Core;
using RentingManager.Server.Core.Domain;

namespace RentingManager.Server.Data
{
    public interface IRentRepository : IRepository<Rent>
    {
        Rent GetWithRentItemsAndPaymentsById(int id);

        Rent GetWithRentItemsById(int id);

        IPagedList<Rent> GetPagedList(int customerId, string referenceNumber, int status, int pageIndex, int pageSize);
    }
}
