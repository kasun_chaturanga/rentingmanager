﻿using RentingManager.Server.Core.Domain;
using System;

namespace RentingManager.Server.Util
{
    public static class RentCalculationHelper
    {
        public static decimal CalculateRent(DateTime rentDate, DateTime returnDate, RentalRateType rateType, decimal rate)
        {
            int noOfUnits = 0;
            switch(rateType)
            {
                case RentalRateType.Daily:
                    noOfUnits = GetNumberOfDays(rentDate, returnDate);
                    break;

                case RentalRateType.Weekly:
                    noOfUnits = GetNumberOfWeeks(rentDate, returnDate);
                    break;

                case RentalRateType.Monthly:
                    noOfUnits = GetNumberOfMonths(rentDate, returnDate);
                    break;
            }

            return rate * noOfUnits;
        }

        private static int GetNumberOfDays(DateTime rentDate, DateTime returnDate)
        {
            double actualDays = returnDate.Subtract(rentDate).TotalDays;
            int roundedDays = (int)actualDays;
            if (actualDays > roundedDays)
            {
                roundedDays = roundedDays + 1;
            }

            return roundedDays;
        }

        private static int GetNumberOfWeeks(DateTime rentDate, DateTime returnDate)
        {
            double actualWeeks = returnDate.Subtract(rentDate).TotalDays / 7;
            int roundedWeeks = (int)actualWeeks;
            if (actualWeeks > roundedWeeks)
            {
                roundedWeeks = roundedWeeks + 1;
            }

            return roundedWeeks;
        }

        private static int GetNumberOfMonths(DateTime rentDate, DateTime returnDate)
        {
            return 0;
        }
    }
}
