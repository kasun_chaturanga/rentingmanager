﻿namespace RentingManager.Server.Core.Domain
{
    public enum RentStatus
    {
        Created = 1,

        Issued = 2,

        Returned = 3
    }
}
