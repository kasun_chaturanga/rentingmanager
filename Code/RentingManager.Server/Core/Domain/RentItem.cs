﻿using System;

namespace RentingManager.Server.Core.Domain
{
    public class RentItem : EntityBase
    {
        public int RentId { get; set; }

        public int ItemId { get; set; }

        public RentalRateType RateType { get; set; }

        public decimal GivenRate { get; set; }

        public DateTime RentingDate { get; set; }

        public DateTime ReturnDate { get; set; }
    }
}
