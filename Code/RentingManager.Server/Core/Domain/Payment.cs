﻿using System;

namespace RentingManager.Server.Core.Domain
{
    public class Payment : EntityBase
    {
        public int RentId { get; set; }

        public decimal Amount { get; set; }

        public DateTime Date { get; set; }

        public string Description { get; set; }
    }
}
