﻿using System.Collections.Generic;

namespace RentingManager.Server.Core.Domain
{
    public class Rent : EntityBase
    {
        public Rent()
        {
            RentItems = new List<RentItem>();
            Payments = new List<Payment>();
        }

        public string ReferenceNumber { get; set; }

        public int CustomerId { get; set; }

        public string Description { get; set; }

        public RentStatus Status { get; set; }

        public IList<RentItem> RentItems { get; set; }

        public IList<Payment> Payments { get; set; }
    }
}
