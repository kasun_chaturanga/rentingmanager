﻿namespace RentingManager.Server.Core.Domain
{
    public enum RentalRateType
    {
        Daily = 1,

        Weekly = 2,

        Monthly = 3
    }
}
