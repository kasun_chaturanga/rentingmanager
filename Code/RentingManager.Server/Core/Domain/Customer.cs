﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RentingManager.Server.Core.Domain
{
    public class Customer : EntityBase
    {
        [Required]
        public string Name { get; set; }

        public string Nic { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }
    }
}
