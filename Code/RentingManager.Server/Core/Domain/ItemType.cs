﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RentingManager.Server.Core.Domain
{
    public class ItemType : EntityBase
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
