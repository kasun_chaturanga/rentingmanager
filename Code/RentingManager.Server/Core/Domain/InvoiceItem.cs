﻿namespace RentingManager.Server.Core.Domain
{
    public class InvoiceItem
    {
        public RentItem Item { get; set; }

        public decimal RentAmount { get; set; }
    }
}
