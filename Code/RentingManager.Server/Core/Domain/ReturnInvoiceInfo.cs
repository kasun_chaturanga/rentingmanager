﻿using System.Collections.Generic;

namespace RentingManager.Server.Core.Domain
{
    public class ReturnInvoiceInfo
    {
        public ReturnInvoiceInfo()
        {
            Items = new List<InvoiceItem>();
        }

        public string ReferenceNumber {get; set;}

        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        public IList<InvoiceItem> Items { get; set; }

        public IList<Payment> Payments { get; set; }
    }
}
