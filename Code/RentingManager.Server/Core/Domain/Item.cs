﻿using System.Collections.Generic;

namespace RentingManager.Server.Core.Domain
{
    public class Item : EntityBase
    {
        public Item()
        {
            Rates = new List<RentalRate>();
        }

        public int TypeId { get; set; }

        public string Name { get; set; }

        public string IdetificationNumber { get; set; }

        public string ModelNumber { get; set; }

        public string Description { get; set; }

        public IList<RentalRate> Rates { get; set; }
    }
}
