﻿namespace RentingManager.Server.Core.Domain
{
    public class RentalRate : EntityBase
    {
        public int ItemId { get; set; }

        public RentalRateType RateType { get; set; }

        public decimal Rate { get; set; }
    }
}
